import { User } from "../../domain/entities/user.js";
import { UserModel } from "./schema/user.js";
export class UserRepository {
  async registerUser(user) {
    //Check the email is taken

    const userExists = await User.findOne({ email: user.email });
    if (userExists) {
      res.status(400);
      throw new Error("User already exists");
    }
    //Hash the user password
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(user.password, salt);

    //create the user
    const newUser = new UserModel({
      username,
      password: hashedPassword,
      email,
    });
    //Add the date the trial will end
    newUser.trialExpires = new Date(
      new Date().getTime() + newUser.trialPeriod * 24 * 60 * 60 * 1000
    );

    //Save the user
    await newUser.save();
  }

  async findUser(username) {
    return UserModel.findOne({ username: username });
  }
}
