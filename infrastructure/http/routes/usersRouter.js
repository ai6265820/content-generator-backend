import express from "express";
// const {
//   register,
//   login,
//   logout,
//   userProfile,
//   checkAuth,
// } = require("../controllers/usersController");
//const isAuthenticated = require("../middlewares/isAuthenticated");
//const { verifyPayment } = require("../controllers/handleStripePayment");
import { getRegisterHandler } from "../handlers/userHandler.js";

// usersRouter.post("/login", login);
// usersRouter.post("/logout", logout);
// usersRouter.get("/profile", isAuthenticated, userProfile);
// usersRouter.get("/auth/check", isAuthenticated, checkAuth);

export const getUserRouter = (userUseCase) => {
  const usersRouter = express.Router();

  usersRouter.post("/register", getRegisterHandler(userUseCase));
  return usersRouter;
};
