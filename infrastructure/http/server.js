import express from "express";
import dotenv from "dotenv";
import cors from "cors";
import cookieParser from "cookie-parser";
import { getUserRouter } from "./routes/usersRouter.js";
import { errorHandler } from "./middlewares/errorMiddleware.js";
//const { errorHandler } = require("./middlewares/errorMiddleware");
//const openAIRouter = require("./routes/openAIRouter");
//const stripeRouter = require("./routes/stripeRouter");
import { UserRepository } from "./../repositories/userRepo.js";
import { UserUseCase } from "./../../usecases/user.js";
import { connectDB } from "./../repositories/mongo.js";
export const runHttpServer = async () => {
  const app = express();
  const PORT = process.env.PORT || 8090;
  dotenv.config();

  //connect mongo
  await connectDB();
  //----Repository-----
  const userRepo = new UserRepository();

  //-----usecase-----

  const userUseCase = new UserUseCase(userRepo);

  //----middlewares----
  app.use(express.json()); //pass incoming json data
  app.use(cookieParser()); //pass the cookie automatically
  const corsOptions = {
    origin: "http://localhost:3000",
    credentials: true,
  };
  app.use(cors(corsOptions));
  //----Routes-----
  app.use("/api/v1/users", getUserRouter(userUseCase));
  // app.use("/api/v1/openai", openAIRouter);
  // app.use("/api/v1/stripe", stripeRouter);

  //---Error handler middleware----
  app.use(errorHandler);
  //start the server
  app.listen(PORT, console.log(`Server is running on port ${PORT}`));
};
