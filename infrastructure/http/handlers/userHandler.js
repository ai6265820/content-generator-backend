//------Registration-----
export const getRegisterHandler = (userUseCase) => {
  const register = async (req, res) => {
    const { username, email, password } = req.body;
    //Validate
    if (!username || !email || !password) {
      res.status(400);
      throw new Error("Please all fields are required");
    }
    try {
      await userUseCase.CreateUser(username, email, password);
      res.json({
        status: true,
        message: "Registration was successfull",
        user: {
          username,
          email,
        },
      });
    } catch (e) {
      console.log("err", e);
      res.json({
        status: false,
      });
    }
  };
  return register;
};
