export class ContentUseCase {
  constructor(contentRepo, contentGenerator) {
    this.contentRepo = contentRepo;
    this.contentGenerator = contentGenerator;
  }

  async GenerateContent(userID, prompt) {
    const content = await this.contentGenerator.GenerateContent(prompt);

    await this.contentRepo.SaveUserContent(userID, prompt);
  }
}
