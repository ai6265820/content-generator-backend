import { User } from "../domain/entities/user.js";

export class UserUseCase {
  constructor(userRepo) {
    this.userRepo = userRepo;
  }

  async CreateUser(username, email, password) {
    const existingUser = await this.userRepo.findUser(username);
    if (!existingUser) {
      console.log("user already exist");
      throw new Error("User already exists");
    }

    const user = new User(username, email, password);
    await this.userRepo.registerUser(user);
    return true;
  }

  async GetUser(username) {
    const user = await this.userRepo.findUser(username);
    if (!user) {
      console.log("user not found", err);
      return err;
    }
    return user;
  }

  async UpgradePlan(userID, plan) {
    await this.userRepo.UpgradePlan(userID, plan);
  }
}
